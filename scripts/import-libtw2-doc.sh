#!/bin/bash

if [ -z "$1" ]; then
	OUT_DIR="../content/encodings/"
else
	OUT_DIR="$1"
fi

INDEX="${OUT_DIR}/_index.md"

function import() {
	local REMOTE_NAME="$1"
	local TITLE="$2"
	FILENAME="${OUT_DIR}/${REMOTE_NAME}"
	echo "importing ${REMOTE_NAME} to ${FILENAME}"

	echo "---" > $FILENAME
	echo "title: \"${TITLE}\"" >> $FILENAME
	echo "---" >> $FILENAME
	echo ""    >> $FILENAME
	echo "<!-- File imported from https://github.com/heinrich5991/libtw2/blob/master/doc/${REMOTE_NAME}. -->"
	echo "<!-- Please create pull requests at https://github.com/heinrich5991/libtw2 if you want to edit this page. -->" >> $FILENAME
	echo ""    >> $FILENAME
	echo '{{% notice note %}}' >> $FILENAME
	echo "This file is mirrored from the [libtw2](https://github.com/heinrich5991/libtw2) documentation and is dual-licensed under MIT or APACHE." >> $FILENAME
	echo '{{% /notice %}}' >> $FILENAME
	echo ""    >> $FILENAME

	curl "https://raw.githubusercontent.com/heinrich5991/libtw2/master/doc/${REMOTE_NAME}" \
		| pandoc --from gfm --to commonmark \
		| sed 's/\.md)/)/g' \
		| sed 's/^#/##/g'  >> $FILENAME # make headings subheadings

	# add a link to the index page
	echo " * [${TITLE}](${REMOTE_NAME/.md/})" >> $INDEX
}

function file_names() {
	echo connection.md,Connection
	echo datafile.md,Datafile
	echo demo.md,Demo
	echo huffman.md,Huffman
	echo int.md,Int
	echo packet.md,Packet
	echo quirks.md,Quirks
	echo serverinfo_extended.md,Serverinfo extended
	echo snapshot.md,Snapshot
	echo teehistorian.md,Teehistorian
}

function generate_index() {
	echo "---" > $INDEX
	echo "title: \"Encodings\"" >> $INDEX
	echo "weight: 90" >> $INDEX
	echo "---" >> $INDEX
	echo ""    >> $INDEX
	echo "<!-- This is an auto generated file. If you want to make changes edit scripts/import-twmap2-doc.sh instead -->" >> $INDEX
	echo ""    >> $INDEX
	echo '{{% notice note %}}' >> $INDEX
	echo "This section is mirrored from the [libtw2](https://github.com/heinrich5991/libtw2) documentation and is dual-licensed under MIT or APACHE." >> $INDEX
	echo '{{% /notice %}}' >> $INDEX
	echo ""    >> $INDEX
	echo "Technical documentation of Teeworlds file formats and network protocol" >> $INDEX
	echo ""    >> $INDEX
}

mkdir -p "$OUT_DIR"

# regenerate index page
generate_index

IFS=','
file_names | while read NAME TITLE
do
	import $NAME $TITLE
done
