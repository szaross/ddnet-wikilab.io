---
title: "Quirks"
---

<!-- Please create pull requests at https://github.com/heinrich5991/libtw2 if you want to edit this page. -->

{{% notice note %}}
This file is mirrored from the [libtw2](https://github.com/heinrich5991/libtw2) documentation and is dual-licensed under MIT or APACHE.
{{% /notice %}}

  - Huffman compression has an extra byte if the actual end is at a byte
    boundary.
  - The size field has a weird splitting in the chunk header.
  - The ack field of the packet header is 12 instead of 10 bit wide.
  - The sequence field of the chunk header has overlapping bits.
  - The client doesn't care about the actual lengths of the snapshot
    chunks, it'll always pad to `MAX_SNAPSHOT_PADSIZE`.
  - The client wants to receive the last snapshot part last, otherwise
    the resulting delta is too long.
  - CCharacterCore has unused fields `m_HookDx`, `m_HookDy`
