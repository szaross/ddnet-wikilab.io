---
title: "Connection"
---

<!-- Please create pull requests at https://github.com/heinrich5991/libtw2 if you want to edit this page. -->

{{% notice note %}}
This file is mirrored from the [libtw2](https://github.com/heinrich5991/libtw2) documentation and is dual-licensed under MIT or APACHE.
{{% /notice %}}

``` 
  -> s:info
  <- s:map_change <----------------------+
[ -> s:request_map_data ]                |
[ <- s:map_data ]                        |
  -> s:ready                             |
[ <- g:sv_motd ]                         |
  <- s:con_ready                         |
  -> g:client_start_info                 |
[ <- g:sv_vote_clear_options ]           |
[ <- g:sv_tune_params ]                  |
  <- g:sv_ready_to_enter                 |
  -> s:enter_game                        |
  ingame --------------------------------+
```
